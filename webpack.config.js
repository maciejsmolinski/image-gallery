module.exports = {
  entry: './src/js/main.js',
  output: {
    filename: './dest/js/main.js',
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
            'es2040'
          ],
        },
      },
    ],
  },
};
