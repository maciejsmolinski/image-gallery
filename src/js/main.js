// Import Gallery component definition, define in window
window.customElements.define('app-gallery', require('./components/gallery'));


// Query the API and render gallery
require('./utils/api')
  .gallery()
  .then(response => {
    // Find an existing gallery or insert a new one
    const gallery = document.querySelector('app-gallery') || document.body.appendChild(document.createElement('app-gallery'));

    // Assign images
    gallery.images = response.pictures;
  })
  .catch(error => console.error(error));
