# Image Gallery

Custom Elements (native) based image gallery supporting drag&drop reordering.

![](http://g.recordit.co/8qnvHzS2Cg.gif)

## Requirements

Node.js is required in order to build and run the application.
Preferred 6.x.x version.
You can download Node.js from https://nodejs.org/en/

## Setup

* Clone the repository and change directory: `git clone git@bitbucket.org:maciejsmolinski/image-gallery.git && cd image-gallery`
* Install dependencies with `npm install`
* Compile all assets once with `npm run build` or compile and watch for file changes with `npm run watch`
* Run the application server under [http://localhost:4242](http://localhost:4242) with `npm start`

## Setup - More Options

* To run the server on a specified port, run `PORT=<port> npm start`, e.g. `PORT=3131 npm start`
* To make your server reload whenever files change, it is recommended to use `nodemon` (`nodemon server.js`)

## Assets Pipeline

ES6/ES2015 syntax brings a lot of great improvements, e.g. arrow functions inherit the context therefore writing things like `var self = this;` or `.bind(this)` becomes unnecessary, `const/let` make you think before you define a variable - whether it's going to mutate or not. Destructuring and default parameter values come in handy as well.

Unfortunately, the syntax is not supported widely across the browsers. Bundlers like `webpack` come in handy. They produce ES5 syntax based code out of your ES6 code.

Why not to write ES5 then? ES5 based JS browser engines don't natively support modularity. You can pretty much rely on global variables or Asynchronous Module Bundling (e.g. require.js) only. `Webpack` makes it easy to produce one bundle file out of your well structured small components. Another advantage is that you can fully benefit from writing small modules on back-end side and share your code between front-end and back-end (Node.js).

This is the reason `webpack` bundling is part of the build process.

In order to check the whole build process, please open `gulpfile.js` for details.

## Design Decisions

* `Gallery` is a custom element extending `HTMLElement` (polyfill for older browsers used)
* `Gallery` observes `[images]` attribute in order to re-render things every time property is changed
* `Gallery` is a standalone element
* `Gallery` mutates `[images]` attribute every time drop is made
