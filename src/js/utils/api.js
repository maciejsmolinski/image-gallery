/**
 * Promise-based API.
 *
 * Static JSON data can be replaced with a fetch() call for example
 */
class Api  {
  gallery () {
    return Promise.resolve({
      pictures: [
        { src: '/images/pictures/1.png' },
        { src: '/images/pictures/2.png' },
        { src: '/images/pictures/3.png' },
        { src: '/images/pictures/4.png' },
        { src: '/images/pictures/5.png' },
        { src: '/images/pictures/6.png' },
        { src: '/images/pictures/7.png' },
        { src: '/images/pictures/8.png' },
        { src: '/images/pictures/9.png' },
        { src: '/images/pictures/10.png' },
        { src: '/images/pictures/11.png' },
        { src: '/images/pictures/12.png' },
      ],
    });
  }
}

/**
 * Exposes instance of the API.
 * There's no reason to manage more than one instance.
 */
module.exports = new Api();
