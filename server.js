const port = process.env.PORT || 4242;
const env = process.env.ENV || 'dev';
const serve = require('koa-static');
const server = require('koa')();

// Static Assets Serving. To leverage browser caching, start the server with NODE_ENV=prod
server.use(serve('./dest', env === 'prod' ? { maxage: 1000 * 60 * 10 } : {}));

// Main Request Handler: Serve index.html contents
server.use(function * (next) {
  if (this.req.url === '/') {
    this.body = require('fs').readFileSync('./index.html', { encoding: 'utf-8' });
  }
});

// Listen on previously specified port
server.listen(port, () =>
  console.log(`Server ready and listening for incoming requests on http://localhost:${port}`)
);
