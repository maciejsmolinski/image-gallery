class Gallery extends HTMLElement {
  constructor () {
    super();
    this._bindEventListeners();
  }

  static get observedAttributes() {
    return ["images"];
  }

  attributeChangedCallback(attribute, oldValue, newValue) {
    // Re-render applicaton every time observed attributes change
    this.render();
  }

  get images () {
    return this._parseAttribute(this.getAttribute('images')) || [];
  }

  set images (images) {
    this.setAttribute('images', JSON.stringify(this._parseAttribute(images)));
  }

  /**
   * Moves images between positions
   *
   * Sample Usage:
   *
   *   // Inserts first element into fifth position (0-indexed)
   *   this._moveImage([ 0, 4 ]);
   *
   *   // Inserts third element into first position (0-indexed)
   *   this._moveImage([ 2, 0 ]);
   */
  _moveImage ([fromPosition, toPosition] = [0, 0]) {
    // Escape early if no need to move
    if (fromPosition === toPosition) {
      return;
    }

    // Copy images array, remove current image from the copy, keep all the others
    const otherImages = [ ...this.images ];
    otherImages.splice(fromPosition, 1);

    // Merge parts of the copied array with the selected image in between (at proper position)
    const reorderedImages = [
      ...otherImages.slice(0, toPosition),
      this.images[fromPosition],
      ...otherImages.slice(toPosition)
    ];

    // Mutate the images attribute so that re-render process kicks off
    this.images = reorderedImages;
  }

  /**
   * Binds drag&drop event listeners.
   * Calls `this._moveImage([ fromPosition, toPosition ])` on successful drop
   *
   */
  _bindEventListeners () {
    const isGalleryImage = (element) => element.classList.contains('gallery__image');

    ////////////////////////////////////////////////////////////////////
    //                                                                //
    //       ------------- dataTransfer-----------                    //
    //      |                                     |                   //
    //  DragStart -> DragEnter/DragLeave -> DragOver/Drop -> DragEnd  //
    //                                                                //
    ////////////////////////////////////////////////////////////////////

    this.addEventListener('dragstart', (event) => {
      // Escape early if not a gallery image
      if (!isGalleryImage(event.target)) {
        return;
      }

      event.dataTransfer.effectAllowed = 'move';
      event.dataTransfer.setData('text', event.target.id);

      event.target.classList.add('gallery__image--dragged');
    });

    this.addEventListener('dragenter', (event) => {
      // Escape early if not a gallery image
      if (!isGalleryImage(event.target)) {
        return;
      }

      event.target.classList.add('gallery__image--active');
    });

    this.addEventListener('dragleave', (event) => {
      // Escape early if not a gallery image
      if (!isGalleryImage(event.target)) {
        return;
      }

      event.target.classList.remove('gallery__image--active');
    });

    this.addEventListener('dragover', (event) => {
      // Escape early if not a gallery image
      if (!isGalleryImage(event.target)) {
        return;
      }

      event.preventDefault();
    });

    this.addEventListener('drop', (event) => {
      // Escape early if not a gallery image
      if (!isGalleryImage(event.target)) {
        return;
      }

      // Prevent image drop from redirecting browser to the image url
      event.stopPropagation();
      event.preventDefault();

      const [ fromPostition, toPosition ] = [ event.dataTransfer.getData('text'), event.target.id ];

      // Mutation - reorder images
      this._moveImage([ Number(fromPostition), Number(toPosition) ]);

      event.dataTransfer.clearData();
      event.target.classList.remove('gallery__image--active');
    });

    this.addEventListener('dragend', (event) => {
      // Escape early if not a gallery image
      if (!isGalleryImage(event.target)) {
        return;
      }

      event.target.classList.remove('gallery__image--dragged');
    });
  }

  /**
   * Tries to parse value with JSON.parse(value).
   * Falls back to original value.
   *
   * Sample Usage:
   *
   *   // Non-serialized data (output = input)
   *   this._parseAttribute('Something'); // => 'Something'
   *
   *   // Serialized data (output = parsed input)
   *   this._parseAttribute('{ "enabled": true }'); // => { enabled: true }
   */
  _parseAttribute (value) {
    try {
      return JSON.parse(value);
    } catch (error) {
      return value;
    }
  }

  render () {
    const image = (item, index) => `<img src="${item.src}" id="${index}" class="gallery__image" />`;

    this.innerHTML = `
      <div class="gallery">
        ${this.images.map(image).join('\n')}
      </div>
    `;
  }
}

module.exports = Gallery;
